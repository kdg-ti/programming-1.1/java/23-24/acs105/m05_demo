package be.kdg.pro1.m5.cars;

public class Car {
	private String brand;
	private String color;
	private Engine engine ;
	private int speed;
	private final int MAX_SPEED=350;
	private final int MIN_SPEED=0;

	public Car(){
		engine=new Engine();
	}

	public Car(String brand,String color, int power){
		this();
		//engine=new Engine();
		this.brand=brand;
		this.color=color;
		setPower(power);

	}

	private Engine getEngine() {
		return engine;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public int getSpeed() {
		return speed;
	}

	private void setSpeed(int speed) {
		if(speed >= MIN_SPEED && speed <= MAX_SPEED) {
			this.speed = speed;
		} else{
			System.out.printf("Speed is %d but must be between %d and %d%n",speed,MIN_SPEED,MAX_SPEED);
		}
	}

	public int pressGas(){
		setSpeed(speed + 10);
		return speed;
	}

	public int hitBreak(){
		setSpeed(speed - 10);
		return speed;
	}

	public void setPower(int power){
		engine.setPower(power);
	}

	public int getPower(){
		return getEngine().getPower();
	}

	public String toString(){
		return String.format("Car with brand %s, color %s, engine %s and speed %d%n",
			getBrand(),getColor(), engine,getSpeed());
	}
}
