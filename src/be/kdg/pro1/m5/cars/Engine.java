package be.kdg.pro1.m5.cars;

public class Engine {
	private int power;

	private final int MAX_POWER =300;

	public int getPower() {
		return power;
	}

	@Override
	public String toString() {
		return "Engine{" +
			"power=" + power +
			'}';
	}

	public Engine() {
		this(70);
	}

	public Engine(int power) {
		this.power = power;
	}

	public void setPower(int thePower) {
		if (thePower <= MAX_POWER) {
			power = thePower;
		} else {
			System.out.printf("Maximum power  is %d, ignoring power %d%n", MAX_POWER, thePower);
		}


	}
}
