import be.kdg.pro1.m5.cars.Car;

public class Main {
	public static void main(String[] args) {

		// adding constructor with parameters
//		Car vw = new Car();

//		vw.setBrand("VW"); ;
//		vw.setColor("RED");
//		vw.setPower(75);
		Car vw = new Car("VW","GREEN",90);

		// initialise this in the class
		//vw.engine = new Engine();

		// encapsulating the engine
		//vw.getEngine().setPower(75);




		Car bmw = new Car();
		bmw.setBrand( "BMW");
		bmw.setColor( "BLACK");
		//bmw.engine=new Engine();
		bmw.setPower( 120);

		vw.setPower(1234);

		bmw.setPower(2345);

		vw.pressGas();
		vw.pressGas();
		vw.hitBreak();
		vw.pressGas();
		vw.pressGas();
		vw.hitBreak();
		vw.hitBreak();
		vw.hitBreak();
		vw.hitBreak();
		vw.hitBreak();
		vw.hitBreak();
		vw.pressGas();


// replacing with printing the class's toString
//		System.out.printf("Car with brand %s, color %s, power %d and speed %d%n",
//			vw.getBrand(),vw.getColor(), vw.getPower(),vw.getSpeed());
//		System.out.printf("Car with brand %s, color %s, power %d and speed %d%n",
//			bmw.getBrand(),bmw.getColor(), bmw.getPower(),bmw.getSpeed());
		System.out.println(vw);
		System.out.println(bmw);
	}
}